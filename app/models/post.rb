class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_many :postcategories, dependent: :destroy
  has_many :categories, through: :postcategories
  
  def save_categories(tags)
    current_tags = self.categories.pluck(:name) unless self.categories.nil?
    new_tags = tags - current_tags

    new_tags.each do |new_name|
      postcategory = Category.find_or_create_by(name:new_name)
      self.categories << postcategory
    end
  end
  
  scope :from_category, -> (category_id)  { where(id: post_ids =  Postcategory.where(category_id: category_id).select(:post_id))}
end
