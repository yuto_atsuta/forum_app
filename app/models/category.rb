class Category < ApplicationRecord
  validates :name,presence:true,length:{maximum:50}
  has_many :posts, through: :postcategories
  has_many :postcategories, dependent: :destroy
end
