class PostsController < ApplicationController
  def index
    @posts = Post.all
    @post = Post.new
    
    words = params[:search].to_s.gsub(/(?:[[:space:]%_])+/, " ").split(" ")
    title_query = (["title LIKE ?"] * words.size).join(" AND ")
    body_query = (["body LIKE ?"] * words.size).join(" AND ")
    content_query = (["content LIKE ?"] * words.size).join(" AND ")
    
    title_searched_posts = Post.where(title_query, *words.map { |w| "%#{w}%" })
    body_searched_posts = Post.where(body_query, *words.map { |w| "%#{w}%" })
    content_searched_comments = Comment.where(content_query, *words.map { |w| "%#{w}%" })
    content_searched_posts = content_searched_comments.map(&:post).flatten
    
    @searched_posts = (title_searched_posts + body_searched_posts + content_searched_posts).uniq
  
    if params[:category_id].present?
      @selected_category = Category.find(params[:category_id])
      @posts= Post.from_category(params[:category_id])
    else
      @posts= Post.all
    end
  end

  def show
    @post = Post.find(params[:id])
    @comments = @post.comments
    @comment = Comment.new
  end

  def create
    @post= current_user.posts.build(post_params)
    category_list = params[:category_list].split(",")
    if @post.save
      @post.save_categories(category_list)
      redirect_to root_path, success: "スレッドを作成しました！"
    else
      redirect_back(fallback_location: root_path)
    end
  end
  
  private
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
