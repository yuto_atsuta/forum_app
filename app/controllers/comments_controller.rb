class CommentsController < ApplicationController
  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    @comment.post = Post.find(params[:post_id])
    if @comment.save
      redirect_to post_path(params[:post_id]), success: "コメントを投稿しました！"
    else
      redirect_back(fallback_location: root_path)
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:content)
  end
end

