class CreatePostcategories < ActiveRecord::Migration[5.1]
  def change
    create_table :postcategories do |t|
      t.integer :post_id
      t.integer :category_id

      t.timestamps
    end
    add_index :postcategories, :post_id
    add_index :postcategories, :category_id
    add_index :postcategories, [:post_id,:category_id],unique: true
  end
end
